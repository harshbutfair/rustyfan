use serde::Deserialize;
use std::path::Path;
use std::time::Duration;

#[derive(Deserialize)]
pub struct Config {
    pub read_temperature: String,
    pub temperature_scaling: f64,
    pub write_pwm: String,
    pub min_ramp_down_time: Duration,
    pub fan_speed_setting: Vec<FanSpeedSetting>,
}

#[derive(Deserialize)]
pub struct FanSpeedSetting {
    pub name: String,
    pub pwm: u32,
    pub increase: Option<f64>,
    pub decrease: Option<f64>,
}

impl Config {
    /// Load configuration file with the given filename. If the file does not exist in the current
    /// directory, try changing to the directory containing the executable.
    pub fn load(filename: &str) -> Result<Self, String> {
        // Change to exe directory if config file is not found in current directory.
        let config_path = Path::new(filename);
        if !config_path.exists() {
            Self::change_to_exe_dir()?;
        }

        // Read config file into string.
        let config_str = std::fs::read_to_string(config_path).map_err(|e| e.to_string())?;

        // Parse config file.
        toml::from_str::<Self>(&config_str).map_err(|e| e.to_string())
    }

    /// Change working directory to the directory containing the executable.
    fn change_to_exe_dir() -> Result<(), String> {
        match std::env::current_exe() {
            Ok(path) => path.parent().map_or_else(
                || Err("Config file not found, and current_exe path has no parent.".to_string()),
                |path| std::env::set_current_dir(path).map_err(|e| e.to_string()),
            ),
            Err(e) => Err(format!(
                "Config file not found, and failed to get executable directory: {e}"
            )),
        }
    }

    /// Validate fan speed settings from the config file.
    pub fn validate(&self) -> Result<(), String> {
        let settings = &self.fan_speed_setting;

        // Ensure that the fan_speed_setting vector is not empty.
        if settings.is_empty() {
            return Err("Invalid config - fan_speed_setting missing".to_string());
        }

        // Ensure that there is a way in and out of each speed setting.
        let num_fan_speeds = settings.len();
        // All except the last setting must have an "increase" parameter.
        for setting in settings.iter().take(num_fan_speeds - 1) {
            if setting.increase.is_none() {
                return Err(format!(
                    "Invalid config - no increase parameter defined for {}",
                    setting.name
                ));
            }
        }

        // All except the first setting must have a "decrease" parameter.
        for setting in settings.iter().skip(1) {
            if setting.decrease.is_none() {
                return Err(format!(
                    "Invalid config - no decrease parameter defined for {}",
                    setting.name
                ));
            }
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_empty() {
        let conf = Config {
            read_temperature: "hwmon_device".to_string(),
            write_pwm: "pwm_device".to_string(),
            temperature_scaling: 0.001,
            min_ramp_down_time: Duration::from_secs(0),
            fan_speed_setting: vec![],
        };
        assert!(conf.validate().is_err());
    }

    #[test]
    fn test_missing_increase() {
        // Off -> Low   Never (error)
        // Low -> High  65.0
        // High -> Low  60.0
        // Low -> Off   40.0
        let setting1 = FanSpeedSetting {
            name: "Off".to_string(),
            pwm: 0,
            increase: None,
            decrease: Some(40.0),
        };

        let setting2 = FanSpeedSetting {
            name: "Low".to_string(),
            pwm: 50,
            increase: Some(65.0),
            decrease: Some(40.0),
        };

        let setting3 = FanSpeedSetting {
            name: "High".to_string(),
            pwm: 100,
            increase: None,
            decrease: Some(60.0),
        };

        let conf = Config {
            read_temperature: "hwmon_device".to_string(),
            write_pwm: "pwm_device".to_string(),
            temperature_scaling: 0.001,
            min_ramp_down_time: Duration::from_secs(5),
            fan_speed_setting: vec![setting1, setting2, setting3],
        };
        assert!(conf.validate().is_err());

        // Off -> Low   45.0
        // Low -> High  None (error)
        // High -> Low  60.0
        // Low -> Off   40.0
        let setting1 = FanSpeedSetting {
            name: "Off".to_string(),
            pwm: 0,
            increase: Some(45.0),
            decrease: None,
        };

        let setting2 = FanSpeedSetting {
            name: "Low".to_string(),
            pwm: 50,
            increase: None,
            decrease: Some(40.0),
        };

        let setting3 = FanSpeedSetting {
            name: "High".to_string(),
            pwm: 100,
            increase: None,
            decrease: Some(52.0),
        };

        let conf = Config {
            read_temperature: "hwmon_device".to_string(),
            write_pwm: "pwm_device".to_string(),
            temperature_scaling: 0.001,
            min_ramp_down_time: Duration::from_secs(5),
            fan_speed_setting: vec![setting1, setting2, setting3],
        };
        assert!(conf.validate().is_err());
    }

    #[test]
    fn test_missing_decrease() {
        // Off -> Low   45.0
        // Low -> High  65.0
        // High -> Low  52.0
        // Low -> Off   None (error)
        let setting1 = FanSpeedSetting {
            name: "Off".to_string(),
            pwm: 0,
            increase: Some(45.0),
            decrease: None,
        };

        let setting2 = FanSpeedSetting {
            name: "Low".to_string(),
            pwm: 50,
            increase: Some(65.0),
            decrease: None,
        };

        let setting3 = FanSpeedSetting {
            name: "High".to_string(),
            pwm: 100,
            increase: None,
            decrease: Some(52.0),
        };

        let conf = Config {
            read_temperature: "hwmon_device".to_string(),
            write_pwm: "pwm_device".to_string(),
            temperature_scaling: 0.001,
            min_ramp_down_time: Duration::from_secs(5),
            fan_speed_setting: vec![setting1, setting2, setting3],
        };
        assert!(conf.validate().is_err());

        // Off -> Low   45.0
        // Low -> High  50.0
        // High -> Low  None (error)
        // Low -> Off   40.0
        let setting1 = FanSpeedSetting {
            name: "Off".to_string(),
            pwm: 0,
            increase: Some(45.0),
            decrease: None,
        };

        let setting2 = FanSpeedSetting {
            name: "Low".to_string(),
            pwm: 50,
            increase: Some(50.0),
            decrease: Some(40.0),
        };

        let setting3 = FanSpeedSetting {
            name: "High".to_string(),
            pwm: 100,
            increase: None,
            decrease: None,
        };

        let conf = Config {
            read_temperature: "hwmon_device".to_string(),
            write_pwm: "pwm_device".to_string(),
            temperature_scaling: 0.001,
            min_ramp_down_time: Duration::from_secs(5),
            fan_speed_setting: vec![setting1, setting2, setting3],
        };
        assert!(conf.validate().is_err());
    }

    #[test]
    fn test_valid() {
        // Off -> Low   45.0
        // Low -> High  65.0
        // High -> Low  52.0
        // Low -> Off   40.0
        let setting1 = FanSpeedSetting {
            name: "Off".to_string(),
            pwm: 0,
            increase: Some(45.0),
            decrease: None,
        };

        let setting2 = FanSpeedSetting {
            name: "Low".to_string(),
            pwm: 50,
            increase: Some(65.0),
            decrease: Some(40.0),
        };

        let setting3 = FanSpeedSetting {
            name: "High".to_string(),
            pwm: 100,
            increase: None,
            decrease: Some(52.0),
        };

        let conf = Config {
            read_temperature: "hwmon_device".to_string(),
            write_pwm: "pwm_device".to_string(),
            temperature_scaling: 0.001,
            min_ramp_down_time: Duration::from_secs(5),
            fan_speed_setting: vec![setting1, setting2, setting3],
        };
        assert!(conf.validate().is_ok());
    }
}
