#[macro_use]
extern crate log;
extern crate syslog;

mod config;

use config::Config;
use config::FanSpeedSetting;
use glob::glob;
use log::LevelFilter;
use syslog::{BasicLogger, Facility, Formatter3164};

use std::path::{Path, PathBuf};
use std::time::{Duration, Instant};

const CONFIG_FILE: &str = "rustyfan.toml";
const EXIT_FAILURE: i32 = 1;
const SLEEP_TIME: Duration = Duration::from_millis(1000);

// If we fail to read the temperature for any reason, treat it the same
// as 100 degrees to fail safe.
const MAX_TEMP: f64 = 100.0;

fn main() {
    // Start syslog.
    let formatter = Formatter3164 {
        facility: Facility::LOG_USER,
        hostname: None,
        process: "rustyfan".into(),
        pid: std::process::id(),
    };

    let logger = syslog::unix(formatter).expect("Failed to initialise syslog");
    log::set_boxed_logger(Box::new(BasicLogger::new(logger)))
        .map(|()| log::set_max_level(LevelFilter::Info))
        .expect("Failed to associate syslog with log crate");

    let config = match Config::load(CONFIG_FILE) {
        Ok(config) => config,
        Err(e) => {
            error!("Failed to load configuration file {CONFIG_FILE}: {e}");
            println!("Failed to load configuration file {CONFIG_FILE}: {e}");
            std::process::exit(EXIT_FAILURE);
        }
    };

    // Validate the contents of the config file.
    if let Err(e) = config.validate() {
        error!("{e}");
        println!("{e}");
        std::process::exit(EXIT_FAILURE);
    }

    // Convenience variable for fan speed settings.
    let settings = &config.fan_speed_setting;

    // hwmon devices change numbers sometimes, so support globbing in
    // the configured filenames.
    let temp_path = glob_filename(&config.read_temperature).unwrap_or_else(|()| {
        error!("Failed to glob filename: {}", config.read_temperature);
        println!("Failed to glob filename: {}", config.read_temperature);
        std::process::exit(EXIT_FAILURE);
    });

    let pwm_path = glob_filename(&config.write_pwm).unwrap_or_else(|()| {
        error!("Failed to glob filename: {}", config.write_pwm);
        println!("Failed to glob filename: {}", config.write_pwm);
        std::process::exit(EXIT_FAILURE);
    });

    // Assume the first fan speed setting to begin with.
    let mut idx: usize = 0;
    if set_fan_speed(
        &pwm_path,
        settings,
        idx,
        "Initialising",
        read_temp(&temp_path, config.temperature_scaling),
    )
    .is_err()
    {
        error!("Failed to set fan speed");
        println!("Failed to set fan speed");
        std::process::exit(EXIT_FAILURE);
    }

    // End of initialisation, it appears that everything will work.

    let max_idx: usize = settings.len() - 1;
    let mut last_increase_time = Instant::now();

    loop {
        let temp = read_temp(&temp_path, config.temperature_scaling);
        debug!(
            "Current temperature {} degrees, increase at {:?}, decrease at {:?}",
            temp, settings[idx].increase, settings[idx].decrease
        );

        if let Some(inc_temp) = settings[idx].increase {
            if temp >= inc_temp
                && idx < max_idx
                && set_fan_speed(&pwm_path, settings, idx + 1, "Increasing", temp).is_ok()
            {
                idx += 1;
                last_increase_time = Instant::now();
            }
        }

        if let Some(dec_temp) = settings[idx].decrease {
            if temp <= dec_temp
                && last_increase_time.elapsed() >= config.min_ramp_down_time
                && idx > 0
                && set_fan_speed(&pwm_path, settings, idx - 1, "Decreasing", temp).is_ok()
            {
                idx -= 1;
            }
        }

        std::thread::sleep(SLEEP_TIME);
    }
}

/// Read temperature as an integer from the given file and multiply it by
/// scaling. Returns `MAX_TEMP` if there is any error reading the temperature.
fn read_temp(path: &Path, scaling: f64) -> f64 {
    let temp_str = match std::fs::read_to_string(path) {
        Ok(s) => s,
        Err(e) => {
            error!("Failed to read temperature: {}", e);
            return MAX_TEMP;
        }
    };

    let unscaled = match temp_str.trim().parse::<i64>() {
        Ok(f) => f,
        Err(e) => {
            error!("Error parsing temperature: {}", e);
            return MAX_TEMP;
        }
    };

    unscaled as f64 * scaling
}

fn set_fan_speed(
    pwm_path: &Path,
    settings: &[FanSpeedSetting],
    idx: usize,
    reason: &str,
    temp: f64,
) -> Result<(), ()> {
    info!(
        "{} fan speed to {}, current temperature {} degrees",
        reason, settings[idx].name, temp
    );

    match std::fs::write(pwm_path, settings[idx].pwm.to_string()) {
        Ok(()) => Ok(()),
        Err(e) => {
            error!("Error writing pwm: {}", e);
            Err(())
        }
    }
}

fn glob_filename(filename: &str) -> Result<PathBuf, ()> {
    match glob(filename) {
        Ok(mut paths) => match paths.next() {
            Some(Ok(path)) => Ok(path),
            Some(Err(e)) => {
                error!("Error globbing {}: {}", filename, e);
                Err(())
            }
            None => {
                error!("No paths matching {}", filename);
                Err(())
            }
        },
        Err(e) => {
            error!("Error globbing {}: {}", filename, e);
            Err(())
        }
    }
}
