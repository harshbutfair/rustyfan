#!/bin/bash

cargo build --release && \
cargo test --release && \
sudo mkdir -p /usr/local/rustyfan && \
sudo cp rustyfan.toml /usr/local/rustyfan/ && \
sudo /etc/init.d/rustyfan stop && \
sudo cp target/release/rustyfan /usr/local/rustyfan/ && \
sudo cp init-script /etc/init.d/rustyfan && \
sudo rc-update add rustyfan boot && \
sudo /etc/init.d/rustyfan start

